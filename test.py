import os
import requests
from flask import Flask,redirect,request
from ch_client import ConsistentHashRing 
app = Flask(__name__)
cr= ConsistentHashRing(1)

@app.route('/v1/expenses', methods=['POST'])
def save():
        args = request.get_json(force=True)
        client=cr[str(args.get('id'))]
        print client
        loc="http://localhost:"+str(client)+"/v1/expenses"
        return redirect(loc, code=307)

@app.route('/v1/expenses/<int:expense_id>', methods=['GET'])
def get(expense_id):
        print "inside"
        client=cr[str(expense_id)]
        print client
        loc="http://localhost:"+str(client)+"/v1/expenses/"+str(expense_id)
        response = requests.get(loc)
        print response.content
        return response.content


if __name__ == '__main__':
       cr["node1"] = "5000"
       cr["node2"] = "5001"
       cr["node3"] = "5002"
       app.run(host='0.0.0.0', port=9090)