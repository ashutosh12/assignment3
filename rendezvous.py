import mmh3
#import socket
import struct


#def ip2long(ip):
 #   """Convert an IP string to long."""
 #   packedIP = socket.inet_aton(ip)
 #   return struct.unpack("!L", packedIP)[0]


def murmur(key):
    """Return murmur3 hash of the key as 32 bit signed int."""
    return mmh3.hash(key)


def weight(node, key):
    a = 1103515245
    b = 12345
    hash = murmur(key)
    return (a * ((a * node + b) ^ hash) + b) % (2^31)


class Ring(object):
    def __init__(self, nodes=None):
        nodes = nodes or {}
        self._nodes = set(nodes)

    def add(self, node):
        self._nodes.add(node)

    def nodes(self):
        return self._nodes

    def remove(self, node):
        self._nodes.remove(node)

    def hash(self, key):
        """Return the node to which the given key hashes to."""
        assert len(self._nodes) > 0
        weights = []
        for node in self._nodes:
            #n = ip2long(node)
            w = weight(node, key)
            weights.append((w, node))

        _, node = max(weights)
        return node

if __name__=='__main__':
    r = Ring()
    r.add(5002)
    r.add(5001)
    r.add(5000)
    client=r.hash("3")
    print client