# CMPE273-EnterpriseDistributedSystems

## Assignment 3
## Steps -

1. Run the three application instances - app.py,app1.py,app2.py and test.py - the proxy code specifically written for this assgn (not reusing previous assgn code).
2. Start three MYSQL instances in docker (done using Kitematic) at following urls-
    a. 192.168.99.100:32784
    b. 192.168.99.100:32783
    c. 192.168.99.100:32782
3. Create database cmpe273 
4. Run the createTable.sql script provided in the assignment folder
5. Now test using Postman
    a. Insert data using url localhost:9090/v1/expenses sending POST Requests
    b. Test insert by making a GET request to localhost:9090/v1/expenses/{expense_id}
 

### Request Response 1

{
  "category": "office supplies", 
  "decision_date": null, 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "100", 
  "id": 1, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 1", 
  "status": null, 
  "submit_date": "2016-12-10"
}


### Request Response 2

{
  "category": "office supplies", 
  "decision_date": null, 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "200", 
  "id": 2, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 2", 
  "status": null, 
  "submit_date": "2016-12-10"
}

### Request Response 3

{
  "category": "office supplies", 
  "decision_date": null, 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "300", 
  "id": 3, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 3", 
  "status": null, 
  "submit_date": "2016-12-10"
}

### Request Response 4

{
  "category": "office supplies", 
  "decision_date": null, 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "400", 
  "id": 4, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 4", 
  "status": null, 
  "submit_date": "2016-12-10"
}

### Request Response 5

{
  "category": "office supplies", 
  "decision_date": null, 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "500", 
  "id": 5, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 5", 
  "status": null, 
  "submit_date": "2016-12-10"
}


### Request Response 6

{
  "category": "office supplies", 
  "decision_date": null, 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "600", 
  "id": 6, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 6", 
  "status": null, 
  "submit_date": "2016-12-10"
}

### Request Response 7

{
  "category": "office supplies", 
  "decision_date": null, 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "700", 
  "id": 7, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 7", 
  "status": null, 
  "submit_date": "2016-12-10"
}


### Request Response 8

{
  "category": "office supplies", 
  "decision_date": null, 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "800", 
  "id": 8, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 8", 
  "status": null, 
  "submit_date": "2016-12-10"
}


### Request Response 9

{
  "category": "office supplies", 
  "decision_date": null, 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "900", 
  "id": 9, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 9", 
  "status": null, 
  "submit_date": "2016-12-10"
}


### Request Response 10

{
  "category": "office supplies", 
  "decision_date": null, 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "1000", 
  "id": 10, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 10", 
  "status": null, 
  "submit_date": "2016-12-10"
}
